#include "myinclude.h"

/******************************************************************************
//固件版本
******************************************************************************/
const uint8_t SOFT_VERSION[] = "IAP-VER1.0";

/******************************************************************************
//回写命令
//命令格式10个字符：*001m0x~~~
//                  0123456789
******************************************************************************/
static void Echo_Command(void) //OK
{
	uart0Buff[COM_CMD_LEN_IDX] = 0; //长度

	//uart0Buff[COM_CMD_MIN_SIZE - 3] = GetXOR(uart0Buff, COM_CMD_MIN_SIZE - 3); //异或 + FLAG1 + FLAG2

	uart0Buff[COM_CMD_MIN_SIZE - 2] = COM_CMD_END_FLAG1; //命令结束符
	uart0Buff[COM_CMD_MIN_SIZE - 1] = COM_CMD_END_FLAG2;

	UART0Send(uart0Buff, COM_CMD_MIN_SIZE);
}

/******************************************************************************
*功能：读取固件版本：2A 51 05 07 A5 00 00 0D 0A
*参数：无
*返回：无
******************************************************************************/
static void Get_Soft_Version(void) //OK
{
	uint8_t ilen;
	
	//ilen = SOFT_VERSION_SIZE;
	ilen = strlen((const char *)SOFT_VERSION);

	uart0Buff[COM_CMD_LEN_IDX] = ilen; //数据长度

	MyStrCPY(uart0Buff + COM_CMD_DATA_IDX, SOFT_VERSION, ilen); //数据

	//uart0Buff[ilen + COM_CMD_MIN_SIZE - 3] = GetXOR(uart0Buff, ilen + COM_CMD_MIN_SIZE - 3); //xor检验位

	uart0Buff[ilen + COM_CMD_MIN_SIZE - 2] = COM_CMD_END_FLAG1; //命令结束符
	uart0Buff[ilen + COM_CMD_MIN_SIZE - 1] = COM_CMD_END_FLAG2;

	UART0Send(uart0Buff, ilen + COM_CMD_MIN_SIZE);
}

/******************************************************************************
*功能：擦除扇区：2A 51 05 07 51 00 00 0D 0A
*参数：无
*返回：无
******************************************************************************/
static void Com_Erase_Sector(void)
{
	if (iap_erase())
	{
		Echo_Command();
	}
}

/******************************************************************************
*功能：接收IAP数据
*参数：无
*返回：无
******************************************************************************/
static void Get_IAP_Data(void)
{
	if (iap_data(uart0Buff+COM_CMD_DATA_IDX, uart0cnt-COM_CMD_MIN_SIZE))
	{
		Echo_Command();
	}
}

/******************************************************************************
*功能：结束IAP
*参数：无
*返回：无
******************************************************************************/
static void End_IAP_Data(void)
{
	if (end_iap())
	{
		Echo_Command();
	}
}

/******************************************************************************
*功能：处理串口0命令
*参数：无
*返回：无
******************************************************************************/
void ProcessUART0(void)
{
	//PrintHexByRTT(uart0Buff, uart0cnt);

    if (uart0cnt < COM_CMD_MIN_SIZE)
        goto lbEND;
    
	switch (uart0Buff[COM_CMD_IDX]) //处理命令
	{
		case COM_CMD_SOFT_VERSION: //读取固件版本
            Get_Soft_Version();
        break;
		
		case COM_CMD_ERASE_SECTOR: //擦除扇区
            Com_Erase_Sector();
        break;
        
		case COM_CMD_IAP_DATA: //接收IAP数据
            Get_IAP_Data();
        break;
		
		case COM_CMD_END_DATA: //结束IAP
            End_IAP_Data();
        break;		
		
		default: break;
	}
	
lbEND:
	InitUart0Buff();
}
